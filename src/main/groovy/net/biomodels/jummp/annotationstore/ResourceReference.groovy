/**
 * Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.annotationstore

import grails.persistence.Entity

/**
 * Represents a reference to a resource, i.e. an annotation.
 *
 * @author Raza Ali
 * @author Mihai Glonț <mihai.glont@ebi.ac.uk>
 * @version 20151207
 */
@Entity
public class ResourceReference implements Serializable {
    String datatype
    String collectionName
    String name
    String uri
    String accession
    String shortName
    String description
    Set synonyms = []
    Set parents = []
    static hasMany = [parents: ResourceReference, statements: Statement, synonyms: String]

    static mapping = {
        description type: 'text'
        synonyms lazy: false
        parents lazy: false
        name type: 'text'
    }

    static constraints = {
        uri nullable: true
        shortName nullable: true, blank: false
        description nullable: true, blank: false
        name nullable: true, blank: false
        accession nullable: true, blank: false
        collectionName nullable: true, blank: false
    }

    public void addParent(ResourceReference parent) {
        parents.add(parent)
    }

    public void addSynonym(String synonym) {
        synonyms.add(synonym)
    }
}

